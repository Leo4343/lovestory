package com.ice.love.story.entities;

import com.ice.love.story.meetings.dto.MeetingDTO;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.UUID;

import static javax.persistence.CascadeType.*;
import static javax.persistence.FetchType.LAZY;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Meeting implements Serializable {

	@Id
	private UUID id;

	private String name;

	private MeetingType type;

	private ZonedDateTime date;

	private String description;

	private ZonedDateTime created;

	private ZonedDateTime updated;

	@ManyToOne(fetch = LAZY, cascade = {DETACH, MERGE, PERSIST, REFRESH})
	@JoinColumn(name = "group_id", nullable = false)
	private Group group;

	/*@ManyToOne(fetch = LAZY, cascade = {DETACH, MERGE, PERSIST, REFRESH})
	@JoinColumn(name = "account_id", nullable = false)
	private Account creator;*/

	public static Meeting fromDTO(MeetingDTO dto, Group group, Account creator) {
		UUID id = UUID.randomUUID();

		Meeting newMeeting = new Meeting();
		newMeeting.id = id;
		newMeeting.name = dto.getName();
		newMeeting.type = dto.getType();
		newMeeting.date = dto.getDate();
		newMeeting.description = dto.getDescription();
		newMeeting.group = group;
		newMeeting.creator = creator;

		return newMeeting;
	}

	@PrePersist
	private void initCreated() {
		created = ZonedDateTime.now();
	}

	@PreUpdate
	private void refreshUpdated() {
		updated = ZonedDateTime.now();
	}
}
