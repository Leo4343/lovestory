package com.ice.love.story.entities;

import lombok.*;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Getter
@Builder
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
public class Address implements Serializable {

	private String country;
	private String state;
	private String city;
	private String street;
	private String house;
	private String numHouse;
	private String description;

}
