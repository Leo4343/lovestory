package com.ice.love.story.entities;

import com.ice.love.story.accounts.dto.PersonNameDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Getter
@Embeddable
@NoArgsConstructor
public class PersonName implements Serializable {

	private String nick;

	// Имя
	private String firstName;

	// Фамилия
	private String lastName;

	public static PersonName fromPersonNameDTO(PersonNameDTO nameDTO) {
		PersonName newName = new PersonName();
		newName.nick = nameDTO.getNick();
		newName.firstName = nameDTO.getFirstName();
		newName.lastName = nameDTO.getLastName();
		return newName;
	}
}
