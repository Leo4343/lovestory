package com.ice.love.story.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import static javax.persistence.CascadeType.*;

@Getter
@Setter
@Entity
public class Gift implements Serializable {

	@Id
	private UUID id;

	private String description;

	private ZonedDateTime date;

	private Address place;

	private ZonedDateTime created;

	private ZonedDateTime updated;

	@ManyToMany(cascade = {DETACH, MERGE, PERSIST, REFRESH})
	@JoinTable(
			name = "fromAccount_gift",
			joinColumns = @JoinColumn(name = "gift_id"),
			inverseJoinColumns = @JoinColumn(name = "account_id")
	)
	private Set<Account> fromAccounts = new HashSet<>();

	@ManyToMany(cascade = {DETACH, MERGE, PERSIST, REFRESH})
	@JoinTable(
			name = "toAccount_gift",
			joinColumns = @JoinColumn(name = "gift_id"),
			inverseJoinColumns = @JoinColumn(name = "account_id")
	)
	private Set<Account> toAccounts = new HashSet<>();

	@PrePersist
	private void initCreated() {
		created = ZonedDateTime.now();
	}

	@PreUpdate
	private void refreshUpdated() {
		updated = ZonedDateTime.now();
	}
}
