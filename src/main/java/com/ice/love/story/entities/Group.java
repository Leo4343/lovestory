package com.ice.love.story.entities;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Group implements Serializable {

	@Id
	private UUID id;

	@ManyToMany(fetch = LAZY)
	@JoinTable(
			name = "account_group",
			joinColumns = @JoinColumn(name = "group_id"),
			inverseJoinColumns = @JoinColumn(name = "account_id")
	)
	private Set<Account> accounts = new HashSet<>();

	@OneToMany(mappedBy = "group", cascade = ALL, fetch = LAZY)
	private Set<Meeting> meetings = new HashSet<>();

	/*@OneToMany(mappedBy = "group", cascade = ALL, fetch = LAZY)
	private Set<Gift> gifts;*/

	private ZonedDateTime created;

	private ZonedDateTime updated;

	@PrePersist
	private void initCreated() {
		created = ZonedDateTime.now();
	}

	@PreUpdate
	private void refreshUpdated() {
		updated = ZonedDateTime.now();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		Group group = (Group) o;

		return Objects.equals(id, group.id);
	}

	@Override
	public int hashCode() {
		return id != null ? id.hashCode() : 0;
	}
}
