package com.ice.love.story.entities;

import com.ice.love.story.accounts.dto.AccountDTO;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

import static javax.persistence.FetchType.LAZY;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Account implements Serializable {

	@Id
	private UUID id;

	@Embedded
	private PersonName personName;

	private byte[] rights;

	private String email;

	private char[] password;

	private byte[] salt;

	@ManyToMany(mappedBy = "accounts", fetch = LAZY)
	private Set<Group> groups;

	@ManyToMany(mappedBy = "fromAccounts", fetch = LAZY)
	private Set<Gift> giftsFrom;

	@ManyToMany(mappedBy = "toAccounts", fetch = LAZY)
	private Set<Gift> giftsTo;

	private ZonedDateTime created;

	private ZonedDateTime updated;

	public static Account fromDTO(AccountDTO accountDTO) {
		Account newAccount = new Account();
		newAccount.id = UUID.randomUUID();
		newAccount.personName = PersonName.fromPersonNameDTO(accountDTO.getPersonName());
		newAccount.rights = accountDTO.getRights();
		newAccount.email = accountDTO.getEmail();
		return newAccount;
	}

	public void setSecurityData(char[] password, byte[] salt) {
		this.password = password;
		this.salt = salt;
	}

	@PrePersist
	private void initCreated() {
		created = ZonedDateTime.now();
	}

	@PreUpdate
	private void refreshUpdated() {
		updated = ZonedDateTime.now();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		Account account = (Account) o;

		return Objects.equals(id, account.id);
	}

	@Override
	public int hashCode() {
		return id != null ? id.hashCode() : 0;
	}
}
