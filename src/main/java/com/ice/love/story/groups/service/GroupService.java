package com.ice.love.story.groups.service;

import com.ice.love.story.accounts.exception.AccountNotFoundException;
import com.ice.love.story.accounts.repositories.AccountRepository;
import com.ice.love.story.entities.Account;
import com.ice.love.story.entities.Group;
import com.ice.love.story.groups.repository.GroupRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class GroupService {

	private final GroupRepository groupRepository;
	private final AccountRepository accountRepository;

	public void createGroup(UUID creatorId) {
		Optional<Account> accountOpt = accountRepository.findById(creatorId);
		Account account = accountOpt.orElseThrow(AccountNotFoundException::new);

		Group group = new Group();
		group.setCreator(account);

		groupRepository.save(group);
	}
}
