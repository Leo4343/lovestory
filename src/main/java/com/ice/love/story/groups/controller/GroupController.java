package com.ice.love.story.groups.controller;

import org.springframework.http.ResponseEntity;

import java.util.UUID;

public interface GroupController {

	ResponseEntity<Void> createGroup(UUID creatorId);
}
