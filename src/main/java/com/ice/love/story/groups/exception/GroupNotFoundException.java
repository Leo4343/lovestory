package com.ice.love.story.groups.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@ResponseStatus(value = BAD_REQUEST, reason="Группа не найдена")
public class GroupNotFoundException extends RuntimeException {
}
