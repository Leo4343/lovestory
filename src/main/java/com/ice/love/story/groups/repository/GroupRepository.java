package com.ice.love.story.groups.repository;

import com.ice.love.story.entities.Group;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface GroupRepository extends JpaRepository<Group, UUID> {
}
