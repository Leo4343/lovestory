package com.ice.love.story.groups.controller.impl;

import com.ice.love.story.groups.controller.GroupController;
import com.ice.love.story.groups.service.GroupService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@RestController
@RequestMapping("/group")
@RequiredArgsConstructor
public class GroupControllerImpl implements GroupController {

	private final GroupService groupService;

	@Override
	@PostMapping("/create")
	public ResponseEntity<Void> createGroup(@RequestParam @NotNull
			                                      UUID creatorId) {
		groupService.createGroup(creatorId);
		return ResponseEntity.ok().build();
	}
}
