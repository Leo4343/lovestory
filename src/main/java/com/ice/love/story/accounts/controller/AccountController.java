package com.ice.love.story.accounts.controller;

import com.ice.love.story.accounts.dto.AccountDTO;
import org.springframework.http.ResponseEntity;

public interface AccountController {

	ResponseEntity<Void> createAccount(AccountDTO frontAccount);
}
