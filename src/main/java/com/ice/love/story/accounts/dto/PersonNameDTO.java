package com.ice.love.story.accounts.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class PersonNameDTO {

	@NotBlank
	private String nick;

	// Имя
	@NotBlank
	private String firstName;

	// Фамилия
	@NotBlank
	private String lastName;
}
