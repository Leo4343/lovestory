package com.ice.love.story.accounts.controller.impl;

import com.ice.love.story.accounts.controller.AccountController;
import com.ice.love.story.accounts.dto.AccountDTO;
import com.ice.love.story.accounts.services.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/account")
@RequiredArgsConstructor
public class AccountControllerImpl implements AccountController {

	private final AccountService accountService;

	@Override
	@PostMapping("/registration")
	public ResponseEntity<Void> createAccount(@RequestBody @Valid
			                              AccountDTO accountDTO) {
		accountService.createAccount(accountDTO);
		return ResponseEntity.ok().build();
	}
}
