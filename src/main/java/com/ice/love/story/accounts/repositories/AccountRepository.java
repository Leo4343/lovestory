package com.ice.love.story.accounts.repositories;

import com.ice.love.story.entities.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface AccountRepository extends JpaRepository<Account, UUID> {

	@Query(nativeQuery = true, value = "select a from Account a where email=:email or nick=:nick")
	Optional<Account> findAccountByEmailOrNick(@Param("email")
			                                 String email,
	                                          @Param("nick")
			                                 String nick);

}
