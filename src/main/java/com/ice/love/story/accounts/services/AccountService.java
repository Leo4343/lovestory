package com.ice.love.story.accounts.services;

import com.ice.love.story.accounts.dto.AccountDTO;
import com.ice.love.story.accounts.exception.DuplicateEmailException;
import com.ice.love.story.accounts.exception.DuplicateNickException;
import com.ice.love.story.accounts.repositories.AccountRepository;
import com.ice.love.story.entities.Account;
import com.ice.love.story.security.SecurityService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AccountService {

	private final AccountRepository accountRepository;
	private final SecurityService securityService;

	public void createAccount(AccountDTO accountDTO) {
		String email = accountDTO.getEmail();
		String nick = accountDTO.getPersonName().getNick();
		char[] password = accountDTO.getPassword();
		Optional<Account> dublicateAccount = accountRepository.findAccountByEmailOrNick(accountDTO.getEmail(), nick);

		dublicateAccount.ifPresent(d -> throwDublicateException(d, email, nick));

		Account account = Account.fromDTO(accountDTO);
		securityService.tuneSecurityForAccount(account, password);

		accountRepository.save(account);
	}

	private void throwDublicateException(Account account, String email, String nick) {
		if (email.equalsIgnoreCase(account.getEmail())) {
			throw new DuplicateEmailException();
		}

		String DbNick = account.getPersonName().getNick();
		if (DbNick.equalsIgnoreCase(nick)) {
			throw new DuplicateNickException();
		}
	}
}
