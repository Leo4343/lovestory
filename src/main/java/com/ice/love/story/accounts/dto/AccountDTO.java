package com.ice.love.story.accounts.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class AccountDTO {

	@Valid
	private PersonNameDTO personName;

	@Email
	@NotBlank
	private String email;

	@NotBlank
	private char[] password;

	private byte[] rights;
}
