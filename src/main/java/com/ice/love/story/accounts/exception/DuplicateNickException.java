package com.ice.love.story.accounts.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@ResponseStatus(value = BAD_REQUEST, reason="Такой nick уже зарегестрированн")
public class DuplicateNickException extends RuntimeException {
}
