package com.ice.love.story.meetings.repository;

import com.ice.love.story.entities.Meeting;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface MeetingRepository extends JpaRepository<Meeting, UUID> {

}
