package com.ice.love.story.meetings.controller;

import com.ice.love.story.meetings.dto.MeetingDTO;
import org.springframework.http.ResponseEntity;

import java.util.UUID;

public interface MeetingController {

	ResponseEntity<Void> createMeeting(UUID groupId, MeetingDTO meetingDTO);

}
