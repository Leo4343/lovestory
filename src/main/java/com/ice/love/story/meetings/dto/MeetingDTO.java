package com.ice.love.story.meetings.dto;

import com.ice.love.story.entities.MeetingType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;
import java.util.UUID;

@Getter
@Setter
public class MeetingDTO {

	@NotBlank
	private String name;

	@NotNull
	private MeetingType type;

	@NotNull
	private ZonedDateTime date;

	private String description;

	@NotNull
	private UUID creatorId;

}
