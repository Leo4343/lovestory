package com.ice.love.story.meetings.controller.impl;

import com.ice.love.story.meetings.controller.MeetingController;
import com.ice.love.story.meetings.dto.MeetingDTO;
import com.ice.love.story.meetings.service.MeetingService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class MeetingControllerImpl implements MeetingController {

	private final MeetingService meetingService;

	@Override
	@PostMapping("/group/{groupId}/meeting/create")
	public ResponseEntity<Void> createMeeting(@RequestParam
			                                              UUID groupId,
	                                          @RequestBody @Valid
			                                              MeetingDTO meetingDTO) {
		meetingService.createMeeting(groupId, meetingDTO);
		return ResponseEntity.ok().build();
	}
}
