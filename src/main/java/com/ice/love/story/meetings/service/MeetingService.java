package com.ice.love.story.meetings.service;

import com.ice.love.story.accounts.exception.AccountNotFoundException;
import com.ice.love.story.accounts.repositories.AccountRepository;
import com.ice.love.story.entities.Account;
import com.ice.love.story.entities.Group;
import com.ice.love.story.entities.Meeting;
import com.ice.love.story.groups.exception.GroupNotFoundException;
import com.ice.love.story.groups.repository.GroupRepository;
import com.ice.love.story.meetings.dto.MeetingDTO;
import com.ice.love.story.meetings.repository.MeetingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class MeetingService {

	private final MeetingRepository meetingRepository;
	private final GroupRepository groupRepository;
	private final AccountRepository accountRepository;

	public void createMeeting(UUID groupId, MeetingDTO meetingDTO) {
		Optional<Account> creatorOpt = accountRepository.findById(meetingDTO.getCreatorId());
		Optional<Group> groupOpt = groupRepository.findById(groupId);

		Account creator = creatorOpt.orElseThrow(AccountNotFoundException::new);
		Group group = groupOpt.orElseThrow(GroupNotFoundException::new);
		Meeting newMeeting = Meeting.fromDTO(meetingDTO, group, creator);

		meetingRepository.save(newMeeting);
	}
}
