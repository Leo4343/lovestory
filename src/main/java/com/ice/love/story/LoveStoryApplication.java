package com.ice.love.story;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LoveStoryApplication {

	public static void main(String[] args) {
		SpringApplication.run(LoveStoryApplication.class, args);
	}

}
