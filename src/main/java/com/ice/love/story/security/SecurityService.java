package com.ice.love.story.security;

import com.ice.love.story.entities.Account;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class SecurityService {

	public void tuneSecurityForAccount(Account account, char[] password) {

		/*account.setSecurityData(password, salt);

		clearArrays(password, salt);*/
	}

	private void clearArrays(char[] password, byte[] salt) {
		Arrays.fill(password, Character.MIN_VALUE);
		Arrays.fill(salt, Byte.MIN_VALUE);
	}
}
